public class Rectangle {
    int sideA, sideB;
    Point topLeft, topRight, bottomRight, bottomLeft;
    public Rectangle(int sideA, int sideB, int x, int y){
        this.sideA=sideA;
        this.sideB=sideB;
        this.topLeft =  new Point(x,y);
        this.topRight = new Point(x+sideA, y);
        this.bottomRight = new Point(x+sideA, y-sideB);
        this.bottomLeft = new Point(x,y-sideB);
    }
    public int area(){
        return (sideA * sideB);
    }
    public int perimeter(){
        return (sideA*sideB)*2;
    }
    public void corners(){



        System.out.println("Top Left :"  + topLeft);
        System.out.println("Top Right :"  + topRight);
        System.out.println("Bottom Left :" + bottomLeft);
        System.out.println("Bottom Right :" + bottomRight);

    }



}

