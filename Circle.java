public class Circle {
    int radius;
    Point center;
    public Circle(int radius, int x, int y){
        this.radius = radius;
        this.center = new Point(x, y);
    }
    public double area(){
        return (3.14*radius*radius);
    }
    public double perimeter(){
        return (3.14*radius*2);
    }

}