public class Main {
    public static void main(String[] args) {
        Rectangle rec;
        rec = new Rectangle(4,8,2,6);

        System.out.println("Area of rectangle: " + rec.area());
        System.out.println("Perimeter of rectangle: " + rec.perimeter() );

        System.out.println("Coordinates of each corner in rectangle");
        rec.corners();

        Circle circ;
        circ = new Circle(16,4,2);
        System.out.println("Area of circle: " + circ.area());
        System.out.println("Perimeter of circle: " + circ.perimeter() );


    }
}
